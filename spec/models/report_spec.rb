require "rails_helper"
require 'byebug'

describe Report do
  context "Filter" do
    it 'only with dates' do
      create(:report)
      response = Report.filter_by_date('01-05-2018', '02-05-2018')
      expect(response.size).to eq(1)
      expect(response.first.country).to eq('Brazil')
    end

    it 'only fields' do
      create(:report)
      response = Report.filter_by_fields
      expect(response.first.platform).to eq('iOS')
      expect(response.first.country).to eq('Brazil')
    end
  end

  context "Get CPM value" do
    it 'calcule it' do
      create(:report)
      response = Report.first
      cpm = ::Application::Cpm.new.render(response)
      expect(cpm.to_f).to eq(30.39)
    end
  end
end
