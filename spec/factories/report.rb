FactoryBot.define do
  factory :report do
    connection "Test connection"
    app  "App test"
    platform "iOS"
    country "Brazil"
    impressions 110.1
    ad_revenue 3.3456
    date "2018-05-1"
  end
end
