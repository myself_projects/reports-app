Rails.application.routes.draw do
  root 'report#index'

  resources :report, only: [:index]
  get 'report/filter', to: 'report#filter'
end
