module Application
  class Cpm
    def render(report)
      (report.ad_revenue / (report.impressions.to_d / 1000)).round(2)
    end
  end
end
