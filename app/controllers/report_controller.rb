class ReportController < ApplicationController
  before_action :all_fields, only: [:index]
  before_action :dates, only: [:filter]
  before_action :fields, only: [:filter]

  def index
    @reports = Report.all
  end

  def filter
    @reports = @start_date.present? ? filter_with_date : filter_only_fields
    render 'report/index'
  rescue StandardError => e
    puts "Something went wrong with the filters => #{e}"
    redirect_to report_index_url
  end

  private

  def dates
    @start_date = params[:start_date] rescue ''
    @end_date = params[:end_date] rescue ''
  end

  def fields
    @fields = params[:fields].keys rescue all_fields
  end

  def filter_with_date
    Report.filter_by_date(@start_date, @end_date)
  end

  def filter_only_fields
    Report.filter_by_fields
  end

  def without_cpm
    params[:fields].keys - ['cpm']
  end

  def all_fields
    @fields = %w[connection app platform country cpm date]
  end
end
