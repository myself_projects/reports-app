class Report < ApplicationRecord
  scope :filter_by_date, ->(start_date, end_date) { where(date: start_date..end_date) }
  scope :filter_by_fields, -> { Report.all }
end
