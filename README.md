# README

* Ruby version
  -2.5.0

* System dependencies
 - Database:postgresql
 - Rails 5.2.0

* Configuration
- RUN ``` bundle install``` to install all required gems
- Clone the file `.env.development.sample` named the copy as `.env.development` and setting database credentials
 in it (Remember that `REPORT_DB` variable MUST be the reports script database name).

* Database creation
  - You need to run migrations in scripting project

* Test suit
  - Clone the file `.env.development.sample` named the copy as `.env.test` and setting database credentials(not necessary to set `REPORT_DB`)
  - RUN ``` bundle exec rspec ```

* Deployment instructions
 - After run all steps above you just need now to run `rails server` and access your localhost:3000/ad_reports
to see saved reports
